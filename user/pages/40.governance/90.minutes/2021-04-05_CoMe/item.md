---
title: Coordination Meeting
date: "2021-04-05"
taxonomy:
    tag: [come]
---

<!-- CoMe facilitation advice and requirements: https://gitlab.com/kanthaus/kanthaus-governance/-/blob/master/documents/coordinationMeeting/coMeFacilitationAdvice.md -->

# CoMe #185

- Date: 2021-04-05
- Facilitator: janina
- Notary: chandi
- Mika caretaker: Nathalie
- Levi caretaker: Clara
- Physical board caretaker: Talita  
- Digital calendar: Zui
- Reservation sheet purifier: Talita
- Present: Zui, romii, Thore, Larissa, Tilmann, Janina, Clara, Antonin, Talita, chandi

----

<!-- Minute of silence (?) -->

### 0. Check-in round

### 1. Last week review
##### Stats
![usage graph last 90 days](https://codi.kanthaus.online/uploads/upload_57c5660502ce70c19c4dbbf9b3a53a78.png "Usage during the last 90 days")
*(Usage during the last 90 days)*

- **Present:** 14.9 people/day (+1.4)
- **🌡️ Average outdoor temperature:** 9.1 °C (-0.5 °C)
- **⚡ Electricity**
    * usage: 5.5 €/day (⬇️-29%)
    * paid: -1.42 €/day
    * ☀️ self produced: 57% (⬇️-4%)
    * emissions: 5 kg CO₂ₑ/week

- **💧 Water**
    * paid: 3.03 €/day (⬆️+6%)
    * emissions: 2 kg CO₂ₑ/week

- **🔥 Gas**
    * usage: 2.26 €/day (⏬-58%)
    * emissions: 53 kg CO₂ₑ/week


##### Expenditure
- chimney cleaner: 61,94€
- Gerüststellung: 162,70€

##### Income
- ?

##### Corona (_[Framework](https://cloud.kanthaus.online/s/S9HsWHANkmakixr/download)_)
- µCOVIDs available last week: 11441
- µCOVIDs used last week: 503 (really..?)
- µCOVIDS balance from last week: 10938
- µCOVIDs additional this week: 5000
- µCOVIDs available this week: 15938

##### Things that happened
- a lot of stuff got rearranged:
    - new shelves were put into K20 basement, intermediate storage and cave
    - tea and sweets moved to snack kitchen side room
    - cleaning utensils and kitchen spare stuff moved to intermediate storage room
    - well packaged food moved to K20 basement
    - stable table from food storage went to workshop
    - CNC mill moved below the table next to the window in workshop
    - old bed from lantern and mattress from sleep kitchen made new bed in food storage
- more light was installed in K20 basement and snack kitchen
- puzzle mats arrived
- the last beetroot action for last year's harvest took place
- awesome first days of summer with acroyoga and sunbathing in the garden
- the secret support team started mika care and tilmann did a rocket tour
- there was a nice and small sharing event
- social sauna about the upcoming roof weeks, repro and transformation happened
- another trashy movie was screened and enjoyed
- we forgot to put out the organic waste bin last week - it's gonna be gross next time
- we had a nice easter brunch
- janina picked up a new polyester comfort size duvet
- thore and nathalie picked the first bärlauch of the season
- the mystery of the bärlauch cheese is still unsolved...

### 2. This week planning

##### People arriving and leaving
- **Mon.:** Silvan comes back, but will do quarantine in lise's garden
- **Tue.:**
- **Wed.:** Romii leaves
- **Thu.:**
- **Fri.:** kito might come back
- **Sat.:** maybe cousin of zui comes over the day
- **Sun.:**
- **Mon.:**
- **Some day:**
  * Doug returns
  * Franzi might come for an afternoon in the end of the week
  * Eventually building week people will come at the weekend

##### Weather forecast
<!--
https://www.wetteronline.de/wetter/wurzen
https://www.meteoblue.com/en/weather/week/wurzen_germany_2805597?day=3
-->
generally cold, windy and rainy. probably no rain on Friday and Sunday


##### Evaluations and check-ins
- *Thore* _Volunteer_ : _Absolute Days_ threshold 68/60 (+13%)
- *Clara* _Volunteer_ : _Absolute Days_ threshold 61/60 (+2%)


##### [Schedule](https://cloud.kanthaus.online/apps/calendar/)
<!-- Ja&Ti's availability due to Mika care as of 2021-02-08: 13-16, 60% of the time -->
- Monday
    - 10:00 CoMe [janina]
    - 15:00 Sharing Event
- Tuesday
    - 11:00 Thore's evaluation [Fac: Janina]
- Wednesday
    - 10:00 MCM monthly meeting [Fac: Janina]
    - 20:00 Evening Activity: Rap Session [Fac: Zui]
- Thursday
    - 10:00 Power Hour [Fac.: chandi, DJ: zui]
    - 14:00 Building Week Prep meeting [chandi,zui,antonin]
    - 15:00 Consitution Proposal Party [clara, antonin]
    - 18:00 foodsharing Wurzen meeting [Fac: Thore]
- Friday
    - 11:00 Clara's evaluation [Fac: Talita]
    - afternoon: bring down rubble from attic [Fac: Tilmann]
- Saturday
    - yellow bin
- Sunday
- Some day

- Next Monday
  - 10:00 CoMe [Fac: Antonin] -> in Piano Room
- Next week summary
  - Building weeks will start! yeah!

_to be scheduled:_
(*when possible, avoid scheduling events at times that would conflict with being part of the cooking team: 17:00-19:00*)


### 3. Shopping plans
- [Tilmann] 313€ ventilation opening for street side roof https://www.flachkanalmarkt.de/Helios-DDF-315 more details: https://yunity.slack.com/archives/CN9H631BM/p1617033191027000
  - no resistance
- [Tilmann] 9€ siphon replacement for K20-2 bathroom: https://www.obi.de/siphons-fuer-waschbecken/lux-waschtisch-roehrengeruchsverschluss-mit-verstellrohr-38-9-mm-g-1-1-4-weiss/p/4767265
  - no resistance

### 4. Discussion & Announcements
,
#### Round 1
- [clara, antonin] proposal phase for constitutional purpose change of Kanthaus starts today. Shall we schedule a proposal party this week? (presentation: http://pintoch.ulminfo.fr/Constitution/presentation/)
  - Event on Thursday
- [clara] GemOek meeting in Kanthaus after building week in May? 3 more people would come
  - temperature check: okay/support
- [janina] it's bärlauch time! let's do a harvesting & processing action this week!
  - whether is not good this week.
  - interested in picking: zui, romii, larissa, tilmann
  - interested in processing: zui, thore, janina, romii
- [tilmann] move some rubble down from the attic while the scaffolding is still up on the garden side? how about Friday afternoon?
  - some people are motivated! It will happen Friday afternoon
- [chandi] move sharing event slot back to thursday
  - won't be shifted now.
- [zui] building weeks
  - structure information
  - check-in in piano room in the morning
    - no resistance
  - who would host possible visitors? (3 people?)
    - antonin, talita, larissa, janina
  - evening sessions like in the WOI? (not every day)
    - [Janina] Debate Club
    - [chandi] Werewolf
    - [zui] (?) Acroyoga
    - [Larissa] Thai Massage
    - [Antonin] Age Of Empires LAN Party
  - bad weather building projects?
    - [tilmann] fun tasks in the attic
    - [?] yoga room
    - [?] break through K22-1

#### Round 2
- [janina] garden proposal: individual responsibilities for specific plants
  - [janina] I am interested in growing onions, garlic and zucchini
  - more discussion in #kanthaus-garten
- [janina] public blaming: toilet in main bathroom was left clogged again!
  - [janina] we could agree to only pee there and make a new sign..?
- [matthias] Power station for electronics workshop? https://yunity.slack.com/archives/CT0K2AY87/p1617173116002500 https://yunity.slack.com/archives/G7E8RPFMH/p1617200118000700 Needs to be picked up in Halle after making an appointment - when I am not going with the car, can it be picked up on the way or is someone with the car in halle in the next weeks?


### 5. [Task lottery & food planning](https://kanthaus.gitlab.io/dinner-lottery/)

#### Volunteers
- **Mon.:** Tilmann
- **Tue.:**
- **Wed.:**
- **Thu.:**
- **Fri.:**
- **~~Open Tuesday~~:**

#### Unavailabilities
- **Mon.:** Talita, Zui
- **Tue.:** Talita
- **Wed.:** Talita, chandi, Thore, Zui, romii
- **Thu.:** Clara, Thore, romii, chandi
- **Fri.:** Antonin, Thore, romii
- **~~Open Tuesday~~:**
- **Week:** Larissa

#### Lunch cooking this week
- **Mon.:** Tilmann
- **Tue.:** chandi
- **Wed.:** Thore
- **Thu.:** Janina
- **Fri.:**
- **Sat.:**

#### Weekend team this week
- **Participants**:

### 6. For next week
-
