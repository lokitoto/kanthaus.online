---
title: Coordination Meeting
date: "2021-04-12"
taxonomy:
    tag: [come]
---

<!-- CoMe facilitation advice and requirements: https://gitlab.com/kanthaus/kanthaus-governance/-/blob/master/documents/coordinationMeeting/coMeFacilitationAdvice.md -->

# CoMe #186

- Date: 2021-04-12
- Facilitator: Antonin
- Notary: Janina
- Corona commissioner: Chandi
- Mika caretaker: Brezel, Tilmann, Thore
- Levi caretaker: Janina
- Physical board caretaker: Talita
- Digital calendar: Doug
- Reservation sheet purifier: Doug
- Present: Janina, Thore, Nathalie, Larissa, Doug, Antonin, Kito, Chandi, Silvan, Tilmann, Talita, Zui, Clara, Maxime

----

<!-- Minute of silence (?) -->

### 0. Check-in round

### 1. Last week review
##### Stats
![usage graph last 90 days](https://codi.kanthaus.online/uploads/upload_7c7717e654f49bb8f799e3c9a7fd1b54.png "Usage during the last 90 days")
*(Usage during the last 90 days)*

- **Present:** 13.9 people/day (-1.0)
- **⚡ Electricity**
    * usage: 7.42 €/day (⬆️+35%)
    * paid: 0.5 €/day 
    * ☀️ self produced: 65% (⬆️+7%)
    * emissions: 6 kg CO₂ₑ/week

- **💧 Water**
    * paid: 4.17 €/day (⬆️+38%)
    * emissions: 2 kg CO₂ₑ/week

- **🔥 Gas**
    * usage: 5.62 €/day (⏫+149%)
    * emissions: 132 kg CO₂ₑ/week

##### Expenditure
- 313€ for ventilation opening
- 15€ for sink

##### Income
- many lovely things from HZ

##### Corona (_[Framework](https://cloud.kanthaus.online/s/S9HsWHANkmakixr/download)_)
<!-- Update the current probability on the sheet! -->
- µCOVIDs available last week: 15938
- µCOVIDs used last week: 9638
- µCOVIDS balance from last week: 6524
- µCOVIDs additional this week: 5000 
- µCOVIDs available this week: 11524

###### Corona Status
- Numbers compared to the week before
  - 7-day incidence (Germany): 140 per 100.000
      - +25% deaths (~180 per day)
      - +13% intensive care patients
  - 7-day incidence (Leipziger Land): 164 per 100.000
- Curfew currently in place! Relevant exceptions:
  - "die Ausübung [...] ehrenamtlicher Tätigkeiten"
  - "Versorgungsgänge für die Gegenstände des täglichen Bedarfs und der Grundversorgung"
  - "Sport und Bewegung im Freien sowie der Besuch des eigenen oder gepachteten Kleingartens oder Grundstücks"
  - "Zusammenkünfte und Besuche nach § 2 Absatz 1 [2 Haushalte, max 5 Personen]" (was mich total verwirrt)
- Mask mandatory everywhere in public, if it's probable that you might run into someone

Source: https://www.revosax.sachsen.de/vorschrift/19062

##### Things that happened
- some Bärlauch was picked and needs to be processed
- attic cleanup
- mmcm for april happened: roof, plants, yoga room and network architecture simplification teams
- Janina and Kito planted onions in the garden
- KMW is back, repaired and full of food
- lots of nice stuff arrived from Harzgerode
- Zui had a small vacation in Lise's garden
- Maxime and Janina found a doctor in Wurzen who is comfortable speaking English

### 2. This week planning

##### People arriving and leaving
- **Mon.:** 
- **Tue.:** 
- **Wed.:** 
- **Thu.:** 
- **Fri.:** 
- **Sat.:** 
- **Sun.:** 
- **Mon.:** 
- **Some day:** Clara leaves

##### Weather forecast
<!-- 
https://www.wetteronline.de/wetter/wurzen
https://www.meteoblue.com/en/weather/week/wurzen_germany_2805597?day=3
-->
Some rain today and tomorrow, afterwards dry, sometimes sunny and around 10°C max

##### Evaluations and check-ins
- none

##### [Schedule](https://cloud.kanthaus.online/apps/calendar/)
- Monday
    - 10:00 CoMe [antonin]
- Tuesday
    - hausmüll
- Wednesday
- Thursday
    - 10:00 Power Hour [Fac.: antonin, DJ: radio-garden (?)]
- Friday
    - biotonne
- Saturday
- Sunday
- Some day

- Next Monday
  - 10:00 CoMe [Fac: Talita]
- Next week summary

_to be scheduled:_
(*when possible, avoid scheduling events at times that would conflict with being part of the cooking team: 17:00-19:00*)

### 3. Shopping plans
- 1.450€ for 3 roof windows [chandi]
- unknown€ for roof metal parts [doug]

### 4. Discussion & Announcements

#### Round 1
- [tilmann] KMW status
    - technically fine, battery also if turned off properly after usage
    - needs TÜV soon but can wait for Matthias' return
    - handover to Silvan wrt paint issues
    - if we need a new battery we should buy one
    - next person to use it should refill gasoline (but only some)
- [Thore] There is more food in the basement of Christa in Walther-Rathenau-Straße 7. The pickup will be today at 13:30. Who is in to join?
    - Janina wants to! Maybe Larissa will take Levi during the time.
- [kito] I have got lvz+ abo, ask me if you want to read something, maybe I could also share account details
    - Clara has a New York Times account that can also be used.
- [Doug] Missing labeled boxes in short-term storage
    - Nobody knows anything, Doug will reluctantly make new ones
- [Doug] Vortex plus
    - The power hour task of vortex now also includes going through the communal storage areas that have privatized items with labels, take the stuff with missing names and put them in the vortex. The areas are:
        - backpack storage
        - short-term storage
        - towel area in bathroom (not in vortex, but to laundry...^^)
        - private shoe shelf
- [Larissa] Corona framework update: Include testing, vaccination
    - No time for meeting this week as it seems, but small group action is more appreciated anyways.
    - Anybody interested in a small group?
    - Kito is.
- [Clara] Constitution ukuvote proposal phase ends tonight!

#### Round 2
 

### 5. [Task lottery & food planning](https://kanthaus.gitlab.io/task-lottery/)

#### Volunteers
- **Mon.:**
- **Tue.:** 
- **Wed.:** 
- **Thu.:** 
- **Fri.:** 
- **~~Open Tuesday~~:** 

#### Unavailabilities
- **Mon.:** Talita, Antonin, Nathalie, chandi
- **Tue.:** Antonin, Nathalie, chandi
- **Wed.:** kito
- **Thu.:**
- **Fri.:** chandi
- **~~Open Tuesday~~:** 
- **Week:** Clara, Tilmann

#### Lunch cooking this week
- **Mon.:**
- **Tue.:**
- **Wed.:** chandi
- **Thu.:**
- **Fri.:** 
- **Sat.:** 

