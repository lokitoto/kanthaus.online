---
title: Coordination Meeting
date: "2021-03-29"
taxonomy:
    tag: [come]
---

<!-- CoMe facilitation advice and requirements: https://gitlab.com/kanthaus/kanthaus-governance/-/blob/master/documents/coordinationMeeting/coMeFacilitationAdvice.md -->

# CoMe #184

- Date: 2021-03-29
- Facilitator: chandi
- Notary: Silvan
- Mika caretaker: Larissa
- Physical board caretaker: Talita
- Digital calendar: Zui
- Reservation sheet purifier: Thore
- Present: Kerstin, Hannah, Janina, Larissa, Thore, Nathalie, Silvan, Clara, Chandi, Zui, Talita, Tilmann

----

<!-- Minute of silence -->

### 0. Check-in round

### 1. Last week review
<!--  don't read them all out^^ -->
![usage graph last 90 days](https://codi.kanthaus.online/uploads/upload_af6dc3ec461e61865a072b06876cbc82.png "Usage during the last 90 days")
*(Usage during the last 90 days)*

- **Present:** 13.3 people/day (+0.3)
- **🌡️ Average outdoor temperature:** 9.6 °C (+5.4 °C)
- **⚡ Electricity**
    * usage: 7.7 €/day (⬇️-13%)
    * paid: 0.52 €/day 
    * ☀️ self produced: 61% (⬆️+4%)
    * emissions: 6 kg CO₂ₑ/week

- **💧 Water**
    * paid: 2.85 €/day (⬇️-7%)
    * emissions: 2 kg CO₂ₑ/week

- **🔥 Gas**
    * usage: 5.36 €/day (⬇️-27%)
    * emissions: 126 kg CO₂ₑ/week



##### Expenditure
- [antonin] 116.27€ scaffolding security accessories
- [all] 167€ Scaffolding allowance

##### Income
0

##### Corona (_[Framework](https://cloud.kanthaus.online/s/S9HsWHANkmakixr/download)_)
- µCOVIDs available last week: 19232
- µCOVIDs used last week: 12,790,5
- µCOVIDS balance from last week: 6441,5
- µCOVIDs additional this week: 5000
- µCOVIDs available this week: 11441,5
- µCOVIDs/person mean for this week: 

##### Things that happened
- we got our sondernutzungsgenehmigung for the scaffolding (still needs to be transfered)
- Nathalie and Janina met with city officials to progress the food-share point
- we had a nice trip to pödi
- another interesting project updates session took place
- Tilmann made light in the K20 basement and moved a lot of food there
- there were two acroyoga sessions
- chandi hosted a backup party
- silvan and larissa found a garage to rent
- thore met again with heike in wr9 or 7
- tilmann improved the solar power display and included a forecast

### 2. This week planning

##### People arriving and leaving
- **Mon.:** romii comes
- **Tue.:**
- **Wed.:** Hannah and Kerstin leaves
- **Thu.:** 
- **Fri.:** Silvan maybe leaves
- **Sat.:** 
- **Sun.:** 
- **Mon.:** Silvan maybe comes back
- **Some day:** Antonin

##### Weather forecast
<!-- 
https://www.wetteronline.de/wetter/wurzen
https://www.meteoblue.com/en/weather/week/wurzen_germany_2805597?day=3

Please give a summary, dear notary! It's great for future reference.
-->

##### Evaluations and check-ins
- *Thore* _Volunteer_ : _Absolute Days_ threshold 61/60 (+2%)
- *Hannah_FQ20* _Visitor_ : 2 days until _Days Visited_ threshold (21)
- *Kerstin_Move20* _Visitor_ : 2 days until _Days Visited_ threshold (21)


##### Schedule ([In Nextcloud](https://cloud.kanthaus.online/apps/calendar/))
<!-- Ja&Ti's availability due to Mika care as of 2021-02-20: 14-17, 60% of the time -->
- Monday
    - Hausmüll
    - 10:00 CoMe [chandi]
    - 15:00 Sharing event [thore/zui]
- Tuesday
    - 11:00 Hannahs Evaluation [janina]
    - 15:00 Acro Yoga [kerstin,hannah]
- Wednesday
    - 11:00 Kerstin Evaluation [nathalie]
    - afternoon beetroot action 
    - 21:00 !!!!!!!! movie: X-Men: Days of future past [X-Men-Team]
- Thursday
    - Biotonne
    - 10:00 Power Hour [Fac.: thore, DJ: thore] <!-- https://codi.kanthaus.online/powerHourSounds?both -->
    - 15:00 Social sauna [clara]
    - 20:00 Evening Activity: (?) Clowning sessions [zui]
- Friday
- Saturday
- Sunday

- some day:
- Next Monday
    - 10:00 CoMe [janina]
    - 15:00 Sharing Event []
- Next week summary
    - Thores Evaluation
    - Sharing Event at Thursday again

### 3. Shopping plans
- [talita] rapeseed oil, 5l 6€
- [antonin] sondernutzungsgenehmigung for the scaffolding (~167€)

### 4. Discussion & Announcements

#### Round 1
<!-- Is there much food to be washed? -->
- [KH] there is much food to be washed! (much thanks to the dupster diving people)
    - all washed, maaaaany thanks to talita!
- [antonin] should we start renovating the K22 street side roof on Monday 12th April and aim to be finished by Sunday 2nd May?
    - fixed start and aimed finish
- [kerstin] ffj participation of Kanthaus?
    - 11. - 24.10. about 7 people
    - Janina, Clara would host them, think about how they fit in KH
- [chandi] Name signs for private slots in K22 staircase. Is it consensus to have them?
    - use like the towel peg or not at all?
        - use it like the towel pegs
        - you can use unmarked places and put stuff into the vortex
    - at backpack storage one sign is missing
- [janina] andreas visit
    - wants to visit the whole april, kito wants to host her
    - quarantine? teavel? (plane or train through france)
    - general workaway concerns
- [talita] Beetroot/carrots action
    - maybe Wednesday and maybe with mika
- [silvan] why it is slippery in the kitchen and dining room?
    - don´t know, needs to be wiped 

<!-- pause for people who haven't spoken, to think about whether they have a point -->
#### Round 2
- [chandi] Move Sozialsauna on a different date?
    - Nathalie/Janina: Don't move
    - keep social sauna on thursday 15:00

### 5. [Task lottery & food planning](https://kanthaus.gitlab.io/task-lottery/)
Follow it live: https://edumeet.zirkuszelt.org/kanthaus-lottery

#### Volunteers
- **Mon.:** Janina,
- **Tue.:** Hannah, Kerstin
- **Wed.:** 
- **Thu.:** 
- **Fri.:** Larissa, Maxime
- ~~**Sat.:**~~
- ~~**Open Tuesday:**~~

#### Unavailabilities
- **Mon.:** Clara, Chandi, Talita, Zui, Nathalie
- **Tue.:** Clara, Talita, Zui, Nathalie
- **Wed.:** Clara, Talita, Zui, Nathalie
- **Thu.:**
- **Fri.:** 
- ~~**Sat.:**
- ~~**Open Tuesday:**~~
- **Week:** Tilmann, Silvan

#### Weekend team this week
- **Participants**: Nathalie, Clara(?)

### 6. For next week

Expenses:
- chimney cleaner: 61,94€
- Gerüststellung: 162,70€


